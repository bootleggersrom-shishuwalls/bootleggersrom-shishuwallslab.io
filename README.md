# Shishuwalls - Homepage

Welcome to the main site for shishuwalls. If you got here from the website, you might be curious about the whole deal of sending your custom wallpapers, right? Well, in these simple steps we'll teach you how to send a PR with the right changes to include the wallpaper you want! (If we approve it, obviously)

### Step 1: The image
We recommend that the image you submit **respects the 1080p resolution** (as it's the most used resolution nowadays) by adding it into the *walls* folder. Please, remove all spaces on your title (replace them with underscores) and put it on the desired folder, for example, if you want one of your photography shots as a wallpaper, put it inside the photography folder. 

### Step 2: Extra metadata
Inside the folder **_data** we have a file called **wallInfo.yml** (It's a YAML file, 85% dumbproof). If you want to specify something like, adding your authorship to the wallpaper, or even putting a fancy title on it, you have to respect the current format. A little example, i want to submit a wallpaper called *Doge's Dementia Denial*, and my file is *doge_denial.jpg*, i have to add the following lines into wallInfo:

    doge_denial:
	    author: r/SexyReddit
	    title: Doge's Dementia Denial

And with that, you'll be done by adding extra metadata to the image, that's all about it.

If you need a commit as a reference about how can you do it, check [this one](https://gitlab.com/bootleggersrom-shishuwalls/bootleggersrom-shishuwalls.gitlab.io/-/commit/a27a70e7ac6d1fbca17ea7661dfd2c229a17b35e).
