---
layout: default
title: ShishuWalls
---
<div class="card shishu-light-bg z-depth-3">
  <div class="card-content">
    <span class="card-title">Welcome to the ShishuWalls!</span>
    <p>This is the landing page for the ShishuWalls, where we mostly get our wallpapers and their info. If you want to learn more, check the <a href="https://gitlab.com/bootleggersrom-shishuwalls/shishuwalls">official repo</a> to learn more.</p>
    <p>For now, we'll upload our main wallpapers, some wallpapers done through the Bootleggers history and some spicy selection from our community best wallpapers. And if it that wasn't enough, you can check our <a href="https://t.me/btlgwalls">community-made wallpapers</a>, they're always bringing up some real good looking wallpapers (Maybe in the future, we'll get a server to parse those wallpapers automatically, who knows).</p>
    <h4>Upload your own wallpaper</h4> 
    <p>For now, you can send a pull request with your desired wallpaper, we can accept bootleggers branded wallpapers or your great shot if you're into mobile photography. Just do a Pull Request on <a href="https://gitlab.com/bootleggersrom-shishuwalls/shishuwalls">our official repo</a> adding your wallpaper to the right category folder, and then editing the file inside <strong>_data/wallInfo.yml</strong> with the proper format. When we approve your wallpaper, it'll show in the right category with the proper credits. Enjoy and thanks for your contributions!</p>
  </div>
</div>